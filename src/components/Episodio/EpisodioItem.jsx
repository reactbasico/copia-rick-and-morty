

export function EpisodioItem({ name, air_date, episode }) {
    return (
        <div className='col-3'>
            <div className='card text-white bg-dark bg-gradient bg-opacity-75 mb-3' style={{ maxWidth: '18rem' }}>
                <div className='card-header text-dark bg-success bg-gradient  '>{air_date}</div>
                <div className='card-body'>
                    <h5 className='card-title'> {name} </h5>
                    <p className='card-text'>
                        {episode}
                    </p>
                </div>
            </div>
        </div>
    )
}