import bannerImg from '../../assets/img/banner-img.jpg';

export function Banner() {
  return (
    <div >
      <img
        style={{ height: '740px', objectFit: 'cover' }}
        src={bannerImg}
        className='card-img '
        alt='banner'
      />
    </div>
  );
}
