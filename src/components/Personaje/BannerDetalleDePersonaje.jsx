function getEstilosStatus(status) {
    let color = 'green';

    if (status == 'unknown') {
        color = 'gray'
    }

    if (status == 'Dead') {
        color = 'red'
    }
    const estiloCirculo = {
        width: '10px',
        height: '10px',
        display: 'inline-block',
        backgroundColor: color,
        borderRadius: '50%',
        marginRight: '10px'

    };
    return estiloCirculo;
}
export function BannerDetallePersonaje({ id, name, status, species, location, image, origin }) {
    return (
        <div className='card bg-dark bg-gradient bg-opacity-75 mb-3'>
            <div className='row g-0'>
                <div className='col-md-4'>
                    <img
                        src={image}
                        className='img-fluid rounded-start'
                        alt={name}
                        style={{ height: '100%', objectfit: 'cover' }}
                    />
                </div>
                <div className='col-md-8' >
                    <div className='card-body'>
                        <h5 className='card-title mb-0 text-white'>{name}</h5>
                        <p className='text-white'>
                            <span
                                style={getEstilosStatus(status)}
                            ></span>
                            {status} - {species}
                        </p>

                        <p className='mb-0 text-warning'>Last Known location</p>
                        <p className='text-white'>{location?.name}</p>

                        <p className='mb-0 text-warning'>Origin :</p>
                        <p className='text-white'>{origin?.name}</p>
                    </div>
                </div>
            </div>
        </div >

    )
}