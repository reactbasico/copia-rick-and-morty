export function Footer() {
  return (
    <div className='navbar-dark bg-dark fixed-bottom'>
      <p className='text-center text-white py-4 mb-0'>
        Proyecto creado por el CAR IV -11/02/2022
      </p>
    </div>
  );
}
