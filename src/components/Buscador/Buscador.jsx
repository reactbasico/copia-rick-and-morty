export function Buscador({ valor, onBuscador }) {
  return (
    <div className='d-flex justify-content-end'>
      <div className='mb-3 col-5'>
        <input
          type='text'
          className='mt-5 form-control bg-warning p-2 text-white bg-gradient'
          placeholder='Buscar personaje...'
          value={valor}
          onChange={(evento) => onBuscador(evento.target.value)}
        />
      </div>
    </div>
  );
}
