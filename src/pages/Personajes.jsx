import { useEffect, useState } from 'react'

import { BannerDetallePersonaje } from '../components/Personaje/BannerDetalleDePersonaje';
import { ListadoEpisodio } from '../components/Episodio/ListadoEpisodio'
import axios from 'axios'
import { useParams } from "react-router-dom"

export function Personajes() {
  let { personajeId } = useParams()
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);
  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api//character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionesEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });
      Promise.all(peticionesEpisodios).then((respuestas) => {
        setEpisodios(respuestas)
      })
    }
  }, [personaje]);

  return (
    <div className='p-5 bg-primary  bg-opacity-25 mb-3' >
      {personaje ? (

        <div>
          <BannerDetallePersonaje {...personaje} />

          <h2 className='py-4 text-white display-3 '><strong>Episodios</strong></h2>
          {episodios ?

            <div>
              <ListadoEpisodio episodios={episodios} />
            </div>

            : <div>Cargando episodios...</div>}

        </div>
      ) : (
        <div>Cargando...</div>
      )}

    </div >

  );
}
