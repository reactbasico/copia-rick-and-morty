import { Banner } from '../components/navBar/Banner';
import { Buscador } from '../components/Buscador/Buscador';
import { ListadoPersonajes } from '../components/Personaje/ListadoDePersonajes';
import { useState } from 'react'

export function Home() {
  let [buscador, setBuscador] = useState('');
  return (
    <div>
      <Banner />
      <div className='py-4 px-4'>
        <h1 className=' display-1 py-5 fw-bolder text-center bg-dark text-white bg-opacity-75 rounded-pill bg-gradient'>
          Personajes Destacados </h1>

        <Buscador valor={buscador} onBuscador={setBuscador} />

        <ListadoPersonajes buscar={buscador} />

      </div>
    </div>
  );
}
