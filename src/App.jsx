import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/App.css';

import { Link, Route, Routes, useParams } from 'react-router-dom';

import { Footer } from './components/Footer/Footer';
import { Header } from './components/navBar/Header';
import { Home } from './pages/Home';
import { Personajes } from './pages/Personajes';

function App() {
  return (
    <div className='App.css'>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/personaje/:personajeId' element={<Personajes />} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
